package com.histler.intech;

import com.google.gson.Gson;
import com.histler.intech.remote.BeelineContentRestService;

import java.lang.ref.WeakReference;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Badr
 * on 29.05.2016 15:23.
 */
public enum AppBeanContainer {
    INSTANCE;

    private WeakReference<RestAdapter> mRestAdapter;
    private WeakReference<BeelineContentRestService> mBeelineContentRestService;

    /*todo move endpoint to properties file, so we can easily change it for debug, if needed*/
    private RestAdapter getRestAdapter() {
        if (mRestAdapter == null || mRestAdapter.get() == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("https://api-content-beeline.intech-global.com/public/marketplaces/1/tags/4/")
                    .setConverter(new GsonConverter(new Gson()))
                    .build();
            mRestAdapter = new WeakReference<>(restAdapter);
        }
        return mRestAdapter.get();
    }

    public BeelineContentRestService getBeelineContentRestService() {
        if (mBeelineContentRestService == null || mBeelineContentRestService.get() == null) {
            mBeelineContentRestService = new WeakReference<>(getRestAdapter().create(BeelineContentRestService.class));
        }
        return mBeelineContentRestService.get();
    }

}
