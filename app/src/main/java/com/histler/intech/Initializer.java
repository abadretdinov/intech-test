package com.histler.intech;

import com.histler.base.BaseBeanContainer;
import com.histler.intech.service.NavigationServiceImpl;

/**
 * Created by Badr
 * on 29.05.2016 16:34.
 */
public final class Initializer {
    protected static void initialize() {
        BaseBeanContainer baseBeanContainer = BaseBeanContainer.INSTANCE;
        baseBeanContainer.setNavigationService(new NavigationServiceImpl());
    }
}
