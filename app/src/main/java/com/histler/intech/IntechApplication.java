package com.histler.intech;

import android.app.Application;

/**
 * Created by Badr
 * on 29.05.2016 16:34.
 */
public class IntechApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Initializer.initialize();
    }
}
