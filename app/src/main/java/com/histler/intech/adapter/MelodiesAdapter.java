package com.histler.intech.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.histler.base.adapter.BasePageableRecyclerAdapter;
import com.histler.intech.R;
import com.histler.intech.adapter.viewholder.MelodyViewHolder;
import com.histler.intech.entity.Melody;

import java.util.List;

/**
 * Created by Badr
 * on 29.05.2016 16:50.
 */
public class MelodiesAdapter extends BasePageableRecyclerAdapter<Melody, MelodyViewHolder> {
    private ListType mType;

    public MelodiesAdapter(List<Melody> data, ListType listType) {
        super(data);
        mType = listType;
    }

    public void setType(ListType listType) {
        mType = listType;
    }

    @Override
    public MelodyViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (mType) {
            default:
            case GRID:
                view = inflater.inflate(R.layout.melody_tile_row, parent, false);
                break;
            case LIST:
                view = inflater.inflate(R.layout.melody_row, parent, false);
                break;
        }
        return new MelodyViewHolder(view, mOnItemClickListener);
    }

    @Override
    public void onBindItemViewHolder(MelodyViewHolder holder, int position) {
        Melody melody = getItem(position);
        holder.setMelody(melody);
    }

    public enum ListType {
        GRID,
        LIST
    }
}
