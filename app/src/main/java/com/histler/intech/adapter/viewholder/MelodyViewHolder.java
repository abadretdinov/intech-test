package com.histler.intech.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.histler.base.adapter.OnItemClickListener;
import com.histler.base.adapter.viewholder.BaseViewHolder;
import com.histler.intech.R;
import com.histler.intech.entity.Melody;

/**
 * Created by Badr
 * on 29.05.2016 16:50.
 */
public class MelodyViewHolder extends BaseViewHolder {
    public ImageView albumView;
    public TextView titleView;
    public TextView artistView;

    public MelodyViewHolder(View itemView, OnItemClickListener clickListener) {
        super(itemView, clickListener);
    }

    @Override
    protected void initView(View itemView) {
        albumView = (ImageView) itemView.findViewById(R.id.album);
        titleView = (TextView) itemView.findViewById(android.R.id.text1);
        artistView = (TextView) itemView.findViewById(android.R.id.text2);
    }

    public void setMelody(Melody melody) {
        if (melody != null) {
            titleView.setText(melody.getTitle());
            artistView.setText(melody.getArtist());
            Glide.with(albumView.getContext()).load(melody.getPicUrl()).into(albumView);
        }
    }
}
