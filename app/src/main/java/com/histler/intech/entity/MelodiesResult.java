package com.histler.intech.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Badr
 * on 29.05.2016 15:39.
 */
public class MelodiesResult implements Serializable {
    private List<Melody> melodies;

    public List<Melody> getMelodies() {
        return melodies;
    }

    public void setMelodies(List<Melody> melodies) {
        this.melodies = melodies;
    }
}
