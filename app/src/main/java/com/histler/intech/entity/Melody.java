package com.histler.intech.entity;

import com.histler.base.entity.IHasId;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Badr
 * on 29.05.2016 15:32.
 */
public class Melody implements Serializable, IHasId {
    private long id;
    private String title;
    private long artistId;
    private String artist;
    private String code;
    private String smsNumber;
    private String price;
    private float fPrice;
    private String purchasePeriod;
    private long position;
    private String picUrl;
    private String demoUrl;
    private List<MelodyTag> tags;
    private boolean active;
    private long relevance;
    private String melodyId;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getArtistId() {
        return artistId;
    }

    public void setArtistId(long artistId) {
        this.artistId = artistId;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSmsNumber() {
        return smsNumber;
    }

    public void setSmsNumber(String smsNumber) {
        this.smsNumber = smsNumber;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public float getfPrice() {
        return fPrice;
    }

    public void setfPrice(float fPrice) {
        this.fPrice = fPrice;
    }

    public String getPurchasePeriod() {
        return purchasePeriod;
    }

    public void setPurchasePeriod(String purchasePeriod) {
        this.purchasePeriod = purchasePeriod;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getDemoUrl() {
        return demoUrl;
    }

    public void setDemoUrl(String demoUrl) {
        this.demoUrl = demoUrl;
    }

    public List<MelodyTag> getTags() {
        return tags;
    }

    public void setTags(List<MelodyTag> tags) {
        this.tags = tags;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getRelevance() {
        return relevance;
    }

    public void setRelevance(long relevance) {
        this.relevance = relevance;
    }

    public String getMelodyId() {
        return melodyId;
    }

    public void setMelodyId(String melodyId) {
        this.melodyId = melodyId;
    }
}
