package com.histler.intech.entity;

import com.histler.base.entity.IHasId;

import java.io.Serializable;

/**
 * Created by Badr
 * on 29.05.2016 15:30.
 */
public class MelodyTag implements Serializable, IHasId {
    private long id;
    private String title;
    private boolean limitedVisibility;
    private long position;
    private boolean isFullCatalogEnabled;
    private long topMelodiesCount;
    private boolean isBlockDisplayMode;

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isLimitedVisibility() {
        return limitedVisibility;
    }

    public void setLimitedVisibility(boolean limitedVisibility) {
        this.limitedVisibility = limitedVisibility;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }

    public boolean isFullCatalogEnabled() {
        return isFullCatalogEnabled;
    }

    public void setFullCatalogEnabled(boolean fullCatalogEnabled) {
        isFullCatalogEnabled = fullCatalogEnabled;
    }

    public long getTopMelodiesCount() {
        return topMelodiesCount;
    }

    public void setTopMelodiesCount(long topMelodiesCount) {
        this.topMelodiesCount = topMelodiesCount;
    }

    public boolean isBlockDisplayMode() {
        return isBlockDisplayMode;
    }

    public void setBlockDisplayMode(boolean blockDisplayMode) {
        isBlockDisplayMode = blockDisplayMode;
    }
}
