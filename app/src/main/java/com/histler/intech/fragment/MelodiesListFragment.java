package com.histler.intech.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.histler.base.adapter.viewholder.BaseViewHolder;
import com.histler.base.fragment.BaseRecyclerFragment;
import com.histler.base.util.Navigate;
import com.histler.base.widget.EndlessRecycleScrollListener;
import com.histler.intech.R;
import com.histler.intech.adapter.MelodiesAdapter;
import com.histler.intech.entity.MelodiesResult;
import com.histler.intech.entity.Melody;
import com.histler.intech.task.MelodiesLoadRequest;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.UncachedSpiceService;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by Badr
 * on 29.05.2016 16:49.
 */
public class MelodiesListFragment extends BaseRecyclerFragment<Melody, BaseViewHolder> implements RequestListener<MelodiesResult>, View.OnClickListener {
    private final SpiceManager mSpiceManager = new SpiceManager(UncachedSpiceService.class);
    EndlessRecycleScrollListener mEndlessScrollListener;
    private MelodiesAdapter.ListType mListType = MelodiesAdapter.ListType.GRID;
    private boolean mReload;

    protected SpiceManager getSpiceManager() {
        return mSpiceManager;
    }

    @Override
    public void onStart() {
        if (!getSpiceManager().isStarted()) {
            getSpiceManager().start(getActivity().getApplicationContext());
            onRefresh();
        }
        super.onStart();
    }

    @Override
    public void onDestroy() {
        if (getSpiceManager().isStarted()) {
            getSpiceManager().shouldStop();
        }
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.melodies, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.view_type) {
            switch (mListType) {
                case GRID:
                    mListType = MelodiesAdapter.ListType.LIST;
                    break;
                case LIST:
                    mListType = MelodiesAdapter.ListType.GRID;
                    break;
            }
            getRecyclerView().setLayoutManager(getLayoutManager(getContext()));
            MelodiesAdapter adapter = (MelodiesAdapter) getAdapter();
            if (adapter != null) {
                adapter.setType(mListType);
                setAdapter(adapter);
            }
            invalidateMenu();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.view_type);
        switch (mListType) {
            case GRID:
                item.setTitle(R.string.view_type_list).setIcon(R.drawable.ic_view_list);
                break;
            case LIST:
                item.setTitle(R.string.view_type_grid).setIcon(R.drawable.ic_view_module);
                break;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean isNeedDivider() {
        return false;
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager(Context context) {
        switch (mListType) {
            case GRID:
                final GridLayoutManager layoutManager = new GridLayoutManager(context, getResources().getInteger(R.integer.columns));
                layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        MelodiesAdapter adapter = (MelodiesAdapter) getAdapter();
                        switch (adapter.getItemViewType(position)) {
                            case MelodiesAdapter.VIEW_TYPE_PROGRESS:
                                return layoutManager.getSpanCount();
                            default:
                                return -1;
                            case MelodiesAdapter.VIEW_TYPE_ITEM:
                                return 1;
                        }
                    }

                    @Override
                    public int getSpanIndex(int position, int spanCount) {
                        return position % spanCount;
                    }
                });
                return layoutManager;
            case LIST:
                return new LinearLayoutManager(context);
        }
        return super.getLayoutManager(context);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mListType == MelodiesAdapter.ListType.GRID) {
            getRecyclerView().setLayoutManager(getLayoutManager(getContext()));
        }
    }

    public void onHardRefresh() {
        setHardRefreshing();
        onRefresh();
    }

    public void loadMelodies(boolean reload) {
        setRefreshing();
        mReload = reload;
        int offset = 0;
        MelodiesAdapter adapter = (MelodiesAdapter) getAdapter();
        if (!mReload && adapter != null) {
            offset = adapter.getItemCount();
            adapter.setLoading(true);
        }
        getSpiceManager().execute(new MelodiesLoadRequest(offset), this);
    }

    @Override
    public void onRefresh() {
        loadMelodies(true);
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position) {
        Melody melody = getAdapter().getItem(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Navigate.PARAM_ENTITY, melody);
        Navigate.to(getContext(), MelodyFragment.class, bundle);
    }

    @Override
    protected String getTitle() {
        return getString(R.string.melodies);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        if (isAdded()) {
            setRefreshed();
            if (getAdapter() != null) {
                ((MelodiesAdapter) getAdapter()).setLoading(false);
            }
            showMessage(getString(R.string.request_with_error));
        }
    }

    @Override
    public void onRequestSuccess(MelodiesResult melodiesResult) {
        if (isAdded()) {
            setRefreshed();
            if (mReload || getAdapter() == null) {
                MelodiesAdapter adapter = new MelodiesAdapter(melodiesResult.getMelodies(), mListType);
                setAdapter(adapter);
                updateScrollListener();
            } else {
                ((MelodiesAdapter) getAdapter()).addData(melodiesResult.getMelodies());
            }
        }
    }

    private void updateScrollListener() {
        if (mEndlessScrollListener != null) {
            getRecyclerView().removeOnScrollListener(mEndlessScrollListener);
        }
        mEndlessScrollListener = new EndlessRecycleScrollListener() {
            @Override
            public void onLoadMore() {
                if (!isRefreshing()) {
                    loadMelodies(false);
                }
            }
        };
        getRecyclerView().addOnScrollListener(mEndlessScrollListener);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.empty_btn) {
            onHardRefresh();
        }
    }
}
