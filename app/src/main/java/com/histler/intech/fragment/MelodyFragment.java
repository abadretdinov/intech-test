package com.histler.intech.fragment;

import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.bumptech.glide.Glide;
import com.histler.base.fragment.BaseFragment;
import com.histler.base.util.Navigate;
import com.histler.intech.R;
import com.histler.intech.entity.Melody;
import com.histler.intech.widget.MediaController;
import com.histler.intech.widget.MediaPlayerControl;

import java.io.IOException;

/**
 * Created by Badr
 * on 30.05.2016 0:33.
 */
public class MelodyFragment extends BaseFragment implements View.OnClickListener, MediaPlayer.OnPreparedListener {
    private Melody mMelody;
    private MediaPlayer mMediaPlayer;
    private MediaController mMediaController;
    private ImageView mAlbumView;

    @Override
    public void onStart() {
        super.onStart();
        if (mMelody == null) {
            initView();
        }
    }

    @Override
    public void onStop() {
        if (mMediaController != null&&mMediaPlayer!=null) {
            mMediaController.doPause();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        releaseMediaPlayer();
        super.onDestroy();
    }

    private void releaseMediaPlayer() {
        if (mMediaController != null) {
            mMediaController.release();
        }
        if (mMediaPlayer != null) {
            try {
                mMediaPlayer.release();
                mMediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void initView() {
        if (getArguments() != null && getArguments().containsKey(Navigate.PARAM_ENTITY)) {
            mMelody = (Melody) getArguments().getSerializable(Navigate.PARAM_ENTITY);
            if (mMelody != null) {
                mMediaController.setTitle(mMelody.getTitle());
                mMediaController.setArtist(mMelody.getArtist());
                Glide.with(mAlbumView.getContext()).load(mMelody.getPicUrl()).into(mAlbumView);
                prepareMediaPlayer();
            }
        } else {
            moveBack();
        }
    }

    private void prepareMediaPlayer() {
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(mMelody.getDemoUrl());
            mMediaPlayer.setOnPreparedListener(this);
            mMediaPlayer.prepareAsync();
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        } catch (IOException e) {
            Log.e(getClass().getName(), e.getMessage());
            showMessage(getString(R.string.mediaplayer_init_error));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.melody_player, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAlbumView = (ImageView) view.findViewById(R.id.album);
        mMediaController = (MediaController) view.findViewById(R.id.media_controller);
    }

    @Override
    protected String getTitle() {
        return null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_pause:

                break;
            case R.id.stop:

                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mMediaController.setMediaPlayer(new MediaPlayerControl(mp));
    }
}
