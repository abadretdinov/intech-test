package com.histler.intech.remote;

import com.histler.intech.entity.MelodiesResult;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Badr
 * on 29.05.2016 15:24.
 */
public interface BeelineContentRestService {
    @GET("/melodies?limit=20")
    MelodiesResult getMelodies(@Query("from") int offset);
}
