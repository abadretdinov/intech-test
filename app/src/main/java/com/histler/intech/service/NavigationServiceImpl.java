package com.histler.intech.service;

import android.support.v4.app.Fragment;

import com.histler.base.activity.BaseActivity;
import com.histler.base.service.NavigationService;
import com.histler.intech.activity.FragmentWrapperActivity;
import com.histler.intech.activity.MainActivity;
import com.histler.intech.fragment.MelodiesListFragment;

/**
 * Created by Badr
 * on 29.05.2016 16:35.
 */
public class NavigationServiceImpl implements NavigationService {
    @Override
    public Class<? extends BaseActivity> getMainActivityClass() {
        return MainActivity.class;
    }

    @Override
    public Class<? extends BaseActivity> getActivityClass() {
        return FragmentWrapperActivity.class;
    }

    @Override
    public Class<? extends Fragment> getDefaultFragment() {
        return MelodiesListFragment.class;
    }
}
