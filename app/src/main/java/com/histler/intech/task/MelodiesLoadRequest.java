package com.histler.intech.task;

import com.histler.intech.AppBeanContainer;
import com.histler.intech.entity.MelodiesResult;
import com.histler.intech.remote.BeelineContentRestService;
import com.octo.android.robospice.request.SpiceRequest;

/**
 * Created by Badr
 * on 29.05.2016 18:29.
 */
public class MelodiesLoadRequest extends SpiceRequest<MelodiesResult> {
    private int mOffset;

    public MelodiesLoadRequest(int offset) {
        super(MelodiesResult.class);
        mOffset = offset;
    }

    @Override
    public MelodiesResult loadDataFromNetwork() throws Exception {

        BeelineContentRestService restService = AppBeanContainer.INSTANCE.getBeelineContentRestService();
        return restService.getMelodies(mOffset);
    }
}
