package com.histler.intech.widget;

import android.media.MediaPlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Badr
 * on 30.05.2016 1:40.
 */
public class MediaPlayerControl implements MediaController.MediaPlayerControl, MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener {
    private MediaPlayer mMediaPlayer;
    private List<OnPlaybackCompleteListener> mOnPlaybackCompleteListeners;
    private int mBufferUpdatePercent;

    public MediaPlayerControl(MediaPlayer mediaPlayer) {
        this.mMediaPlayer = mediaPlayer;
        mediaPlayer.setOnCompletionListener(this);
        mMediaPlayer.setOnBufferingUpdateListener(this);
    }

    @Override
    public void start() {
        mMediaPlayer.start();
    }

    @Override
    public void pause() {
        mMediaPlayer.pause();
    }

    @Override
    public void stop() {
        mMediaPlayer.pause();
        mMediaPlayer.seekTo(0);
    }

    @Override
    public int getDuration() {
        return mMediaPlayer.getDuration();
    }

    @Override
    public int getCurrentPosition() {
        return mMediaPlayer.getCurrentPosition();
    }

    @Override
    public void seekTo(int pos) {
        mMediaPlayer.seekTo(pos);
    }

    @Override
    public boolean isPlaying() {
        return mMediaPlayer.isPlaying();
    }

    @Override
    public int getBufferPercentage() {
        return mBufferUpdatePercent;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    @Override
    public void addOnCompleteListener(OnPlaybackCompleteListener onPlaybackCompleteListener) {
        if (mOnPlaybackCompleteListeners == null) {
            mOnPlaybackCompleteListeners = new ArrayList<>();
        }
        mOnPlaybackCompleteListeners.add(onPlaybackCompleteListener);
    }

    @Override
    public void removeOnCompleteListener(OnPlaybackCompleteListener onPlaybackCompleteListener) {
        if (mOnPlaybackCompleteListeners != null) {
            mOnPlaybackCompleteListeners.remove(onPlaybackCompleteListener);
        }
    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        mBufferUpdatePercent = percent;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (mOnPlaybackCompleteListeners != null) {
            for (OnPlaybackCompleteListener listener : mOnPlaybackCompleteListeners) {
                listener.onComplete();
            }
        }
    }
}
