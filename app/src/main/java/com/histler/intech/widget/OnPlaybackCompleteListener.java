package com.histler.intech.widget;

/**
 * Created by Badr
 * on 30.05.2016 3:12.
 */
public interface OnPlaybackCompleteListener {
    void onComplete();
}
