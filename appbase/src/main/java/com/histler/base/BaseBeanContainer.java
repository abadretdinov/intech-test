package com.histler.base;

import com.histler.base.dao.CreateTableDao;
import com.histler.base.service.NavigationService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Badr
 * on 29.05.2016 15:30.
 */
public enum BaseBeanContainer {
    INSTANCE;
    private final List<CreateTableDao> mAllDaos = new ArrayList<>();
    private NavigationService mNavigationService;

    public NavigationService getNavigationService() {
        return mNavigationService;
    }

    public void setNavigationService(NavigationService navigationService) {
        mNavigationService = navigationService;
    }

    public List<CreateTableDao> getAllDaos() {
        return mAllDaos;
    }

}
