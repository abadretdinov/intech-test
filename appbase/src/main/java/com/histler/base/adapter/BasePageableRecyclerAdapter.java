package com.histler.base.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.histler.base.R;
import com.histler.base.adapter.viewholder.BaseViewHolder;
import com.histler.base.adapter.viewholder.FooterProgressViewHolder;

import java.util.List;

/**
 * Created by Badr
 * on 29.05.2016 16:41.
 */
public abstract class BasePageableRecyclerAdapter<T, VIEW_HOLDER extends BaseViewHolder> extends BaseRecyclerAdapter<T, BaseViewHolder> {
    public static final int VIEW_TYPE_ITEM = 0;
    public static final int VIEW_TYPE_PROGRESS = 1;
    private boolean mIsLoading = false;

    public BasePageableRecyclerAdapter(List<T> data) {
        super(data);
    }

    public void setLoading(boolean isLoading) {
        if (mIsLoading != isLoading) {
            int size = getItemCount();
            mIsLoading = isLoading;
            if (mIsLoading) {
                notifyItemInserted(size);
            } else {
                notifyItemRemoved(size - 1);
            }
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            default:
            case VIEW_TYPE_ITEM:
                return onCreateItemViewHolder(parent, viewType);
            case VIEW_TYPE_PROGRESS:
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View view = inflater.inflate(R.layout.footer_progress_row, parent, false);
                return new FooterProgressViewHolder(view);
        }
    }

    public abstract VIEW_HOLDER onCreateItemViewHolder(ViewGroup parent, int viewType);

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (VIEW_TYPE_PROGRESS != getItemViewType(position)) {
            onBindItemViewHolder((VIEW_HOLDER) holder, position);
        }
    }

    public abstract void onBindItemViewHolder(VIEW_HOLDER holder, int position);

    @Override
    public int getItemCount() {
        if (mIsLoading) {
            return super.getItemCount() + 1;
        } else {
            return super.getItemCount();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mIsLoading && position == getItemCount() - 1) {
            return VIEW_TYPE_PROGRESS;
        }
        return VIEW_TYPE_ITEM;
    }

    public void addData(List<T> data) {
        if (mIsLoading) {
            int size = getItemCount();
            mIsLoading = false;
            notifyItemRemoved(size - 1);
        }
        int position = mData.size();
        int addedSize = 0;
        for (T entity : data) {
            if (!mData.contains(entity)) {
                addedSize++;
                mData.add(entity);
            }
        }
        notifyItemRangeInserted(position, addedSize);
    }
}
