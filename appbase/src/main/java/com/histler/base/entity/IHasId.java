package com.histler.base.entity;

/**
 * Created by Badr
 * on 29.05.2016 15:30.
 */
public interface IHasId {
    long getId();
}
