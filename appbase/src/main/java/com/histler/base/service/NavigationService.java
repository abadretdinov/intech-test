package com.histler.base.service;

import android.support.v4.app.Fragment;

import com.histler.base.activity.BaseActivity;

/**
 * Created by Badr on 28.05.2016.
 */
public interface NavigationService {
    Class<? extends BaseActivity> getMainActivityClass();

    Class<? extends BaseActivity> getActivityClass();

    Class<? extends Fragment> getDefaultFragment();
}
